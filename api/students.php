<?php

$method = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

require __DIR__ . '/connection.php';

function parse_student( array $student )
{
    foreach ( ['id', 'student_number', 'student_age'] as $prop )
        array_key_exists($prop, $student) && ($student[$prop] = intval($student[$prop]));

    return $student;
}

function get_students() : array
{
    global $con;
    $result = $con->query('select * from students');
    $data = $result->fetch_all(MYSQLI_ASSOC);
    return array_map('parse_student', $data);
}

function get_student( int $id ) : array
{
    global $con;
    $stmt = $con->prepare('select * from students where id = ?');
    $stmt->bind_param('d', $id);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = $result->fetch_assoc();
    return parse_student((array) $data);
}

function insert_student( int $student_number, string $student_name, int $student_age ) : int
{
    global $con;
    $stmt = mysqli_prepare($con, 'insert into students (student_number, student_name, student_age) values (?,?,?)');
    mysqli_stmt_bind_param($stmt, 'sss', $student_number, $student_name, $student_age);
    return mysqli_stmt_execute($stmt) ? mysqli_insert_id($con) : 0;
}

function send_json( $data, int $status_code=200 )
{
    header('content-type: application/json');
    http_response_code($status_code);
    echo json_encode($data);
    exit;
}

$request_id = strtolower( "{$method} {$uri}" );

switch ( true ) {
    case $request_id == 'get /students/':
    case $request_id == 'get /students':
        $students = get_students();
        return send_json($students);

    case $request_id == 'post /students/':
    case $request_id == 'post /students':
        $raw_input = file_get_contents('php://input');
        $student = json_decode($raw_input, 1);

        $errors = [];

        if ( empty($student['student_number']) ) {
            $errors['student_number'] = 'This is a required field';
        } else if ( intval($student['student_number']) <= 0 ) {
            $errors['student_number'] = 'Invalid student number';
        }

        if ( empty($student['student_name']) ) {
            $errors['student_name'] = 'This is a required field';
        } else if ( strlen(trim($student['student_name'])) <= 3 ) {
            $errors['student_name'] = 'Invalid student name';
        }

        if ( empty($student['student_age']) ) {
            $errors['student_age'] = 'This is a required field';
        } else if ( intval($student['student_age']) < 18 ) {
            $errors['student_age'] = 'Invalid student age';
        }

        if ( count($errors) > 0 )
            return send_json(['success' => false, 'errors' => $errors], 400);

        $id = insert_student((int) $student['student_number'], trim($student['student_name']), (int) $student['student_age']);

        return send_json(['success' => $id > 0]);

    default:
        preg_match('/^\/students\/(\d+)\/{0,}$/si', $uri, $student_id);
        $student_id = isset($student_id[1]) ? intval($student_id[1]) : null;

        if ( $student_id > 0 ) {
            $student = get_student( $student_id );
            return send_json( $student ? $student : null );
        }

        return send_json('unknonw request');
}