<?php
$servername = 'mysql';
$username = 'students';
$password = 'students';
$database = 'students';

// Create connection
$con = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$con) {
    die("Connection failed: " . mysqli_connect_error());
}
