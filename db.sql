create database if not exists students;

use students;

create table if not exists students (
  id bigint unsigned not null auto_increment,
  student_number int unsigned not null,
  student_name varchar(255) not null,
  student_age int(2) unsigned not null check(student_age >= 18 and student_age <= 90),
  primary key (id)
);
